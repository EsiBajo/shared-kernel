<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Domain\DomainEvent;

use StraTDeS\SharedKernel\Domain\Identity\Id;

abstract class DomainEvent
{
    protected $id;
    protected $userId;
    protected $entityId;
    protected $code;
    protected $version;
    protected $data;
    protected $createdAt;

    protected function __construct(
        Id $id,
        ?Id $userId,
        Id $entityId,
        string $code,
        int $version,
        array $data,
        \DateTime $createdAt
    )
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->entityId = $entityId;
        $this->code = $code;
        $this->version = $version;
        $this->data = $data;
        $this->createdAt = $createdAt;
    }

    /**
     * @param Id $id
     * @param Id $entityId
     * @param array $data
     * @param \DateTime|null $createdAt
     * @param Id|null $userId
     * @return DomainEvent|static
     * @throws \Exception
     */
    final public static function fire(
        Id $id,
        Id $entityId,
        $data = [],
        \DateTime $createdAt = null,
        ?Id $userId = null
    ): DomainEvent {
        return new static(
            $id,
            $userId,
            $entityId,
            static::getDefaultCode(),
            static::getDefaultVersion(),
            $data,
            $createdAt ?? new \DateTime()
        );
    }

    final public function getId(): Id
    {
        return $this->id;
    }

    final public function getUserId(): Id
    {
        return $this->userId;
    }

    final public function getEntityId(): Id
    {
        return $this->entityId;
    }

    final public function getCode(): string
    {
        return $this->code;
    }

    final public function getVersion(): int
    {
        return $this->version;
    }

    final public function getData(): array
    {
        return $this->data;
    }

    final public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    protected abstract static function getDefaultCode(): string;

    protected abstract static function getDefaultVersion(): int;
}