<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Domain\Entity;

use StraTDeS\SharedKernel\Domain\DomainEvent\EventStream;
use PHPUnit\Framework\TestCase;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\DomainEventStub;
use StraTDeS\SharedKernel\Tests\Unit\Domain\DomainEvent\IdStub;

class AggregateRootTest extends TestCase
{
    /**
     * @test
     */
    public function checkRecordThatSavesEventToTheEventStream()
    {
        // Arrange
        $aggregateRoot = new AggregateRootStub(IdStub::generate());
        $event = DomainEventStub::fire(
            IdStub::generate(),
            IdStub::generate(),
            []
        );
        $expectedEventStream = new EventStream();
        $expectedEventStream->addEvent($event);

        // Act
        $aggregateRoot->recordThat($event);

        // Assert
        $this->assertEquals($expectedEventStream, $aggregateRoot->pullEventStream());
    }

    /**
     * @test
     */
    public function checkAggregateRootReturnsProperIdWhenConstructed()
    {
        // Arrange
        $id = IdStub::generate();
        $aggregateRoot = new AggregateRootStub($id);

        // Act

        // Assert
        $this->assertEquals($id, $aggregateRoot->getId());
    }
}
