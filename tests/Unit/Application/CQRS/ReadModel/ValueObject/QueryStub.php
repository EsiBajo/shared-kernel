<?php
/**
 * This file belongs to SharedKernel project.
 *
 * Author: Alex Hernández <info@alexhernandez.info>
 *
 * For license information, view LICENSE file in the root of the project.
 */

namespace StraTDeS\SharedKernel\Tests\Unit\Application\CQRS\ReadModel\ValueObject;

use StraTDeS\SharedKernel\Application\CQRS\ReadModel\ValueObject\Query;

class QueryStub extends Query
{

}